% class to set markers to annotate and interpolate body midline kinematics

classdef MidlineAnnotation < handle
    
properties
    mode_           % 1 = boundary outlines, 2 = images
    n_frames_       % number of frames 
    frame_list_     % n x 1 array of frames in which markers will be set
    images_         % cell array of images (n x m)
    standard_images_% cell array of standard images
    davis_data_     % DaVisProcessing object
    image_scale_    % image scale
    X_              % cell array of X coordinates (n x m)
    Y_              % cell array of Y coordinates (n x m)
    bnd_outlines_   % cell array of (n_bnd_points_ x 2) arrays 
                    % with [x,y] coordinates
    t_              % n_frames_ x 1 time vector
    
    n_markers_          % number of markers along the midline
    n_equi_points_      % number of equidistantly spaced points
    midlines_           % cell array of (n_markers_ x 2) of x,y coord
                        % manually placed
    midlines_equi_      % equidistantly distributed markers 
    midlines_dense_     % dense midline (interpolated between markers)
    midlines_interp_    % interpolated midline profiles (between frames)
    x_                  % x-coordinates of current markers
    y_                  % y-coordinates of current markers
    
    midline_len_        % n_frames_ x 1 vector of midline lengths
    avg_midline_len_    % mean of midline lengths
                    
    midline_plot_      % handle to equidistant marker point plot
    
    current_ax_window_  % 1 x 4 [xmin xmax ymin ymax] size of current ax
end

methods
    %% Constructor 
    % mode:         1 = boundary outlines, 
    %               2 = images
    % input_data:   cell array of either images 
    %               or boundary_outlines (n_frames x 2)
    % t:            time vector (n_frames x 1)
    function obj = MidlineAnnotation(mode, input_data, t, n_marker_points)
        
        if(mode < 4 && length(t)~=length(input_data))
           error('input_data and t must have same length'); 
        end
        
        obj.t_ = t;
        obj.n_frames_ = length(t);
        obj.n_markers_ = n_marker_points;
        obj.mode_ = mode;
        for i = 1:obj.n_frames_
            obj.midlines_{i} = [];
            obj.midlines_interp_{i} = [];
        end
        
        if mode == 1
            % shapes
            LoadShapes(obj,input_data);
        elseif mode == 2
            % images
            LoadImages(obj,input_data);
        elseif mode == 3
            LoadStandardImages(obj,input_data);
        elseif mode == 4
             % load movie
            LoadMovie(obj,input_data);
        elseif mode == 5
            % nothing loaded 
            % (use this mode to import externally obtained markers)
        else
            error('unknown mode! use mode = {1,2}');
        end
        
    end
    
    %% Load movie 
    
    function LoadMovie(obj,movie_path)
        v = VideoReader(movie_path);
        k = 1;
        while hasFrame(v)
            obj.images_{k} = readFrame(v);
            k = k+1;
        end
        obj.n_frames_ = k-1;
    end
    
    %% Load shapes and display to set markers
    % Use body outlines in x-y coordinates to set midline markers
    % boundary_outlines:    cell array of (n_frames x 2) x-y coordinates
    function LoadShapes(obj,boundary_outlines)
        [~,dim] = size(boundary_outlines{1});
        if(dim~=2)
            error(['boundary_outlines should have 2 but has '...
                   num2str(dim) ' columns']);
        end
        obj.bnd_outlines_ = boundary_outlines;        
    end
    
    %% Load images and display to set markers
    % Use entire images along with x-y coordinates to set midline markers
    % images_x_y:   3 x 1 cell array of {images, X, Y}
    %               , where all 3 elements have same dimensions [n x m]
    function LoadImages(obj,images_x_y)
        obj.images_ = images_x_y{1};
        obj.X_ = images_x_y{2};
        obj.Y_ = images_x_y{3};
    end
    
    %% Load standard images from file
    function LoadStandardImages(obj,input_data)
        for i=1:length(input_data)
            obj.standard_images_{i} = input_data{i};
        end
    end
    
    %% Load DaVis data
    function LoadDaVisData(obj,data)
        
        obj.davis_data_ = data;
        
    end
    
    %% Delete DaVis data
    function DeleteDaVisData(obj)
       
        obj.davis_data_ = [];
        
    end
    
    %% Set scale of image
    function SetImageScale(obj,i,calib_len)
        DisplayTrackingObject(obj,i);
        p = drawline;
        x = p.Position(:,1);
        y = p.Position(:,2);
        dist_pix = sqrt((x(1)-x(2))^2+(y(1)-y(2))^2);
        obj.image_scale_ = calib_len/dist_pix;
    end

    %% Set image scale manually
    function SetImageScaleManually(obj, scale)
        obj.image_scale_ = scale;
    end
    
    %% Apply image scale to midline points
    function ApplyImageScale(obj)
        
        for i = 1:length(obj.midlines_interp_)
            obj.midlines_{i} = obj.image_scale_ * ...
                                        obj.midlines_{i};
            obj.midlines_dense_{i} = obj.image_scale_ * ...
                                        obj.midlines_dense_{i};
            obj.midlines_interp_{i} = obj.image_scale_ * ...
                                        obj.midlines_interp_{i};
        end
        
    end
    
    %% Display shapes or images and set markers
    function DefineMidlines(obj,frame_list)
        
        obj.frame_list_ = frame_list;
        
        % initialize figure position
        fig = figure();
        fig.Position = [0 0 1440 1000];
        markersize = 1;
%         CatchKeyPress(obj,fig);
        
        for i = obj.frame_list_(1):obj.frame_list_(end)%obj.frame_list_
            
            % visualization of tracking object
            DisplayTrackingObject(obj,i);
            pause(0.1);
%             ax = gca;
%             xlimits = ax.XLim;
%             ylimits = ax.YLim;
            if(~isempty(find(obj.frame_list_==i,1)))
                hold off;
                DisplayTrackingObject(obj,i);
                MidlinePlotAppearance(obj,markersize);
                
                % visualization of piv image
                hold on;
                %             DisplayDaVisImage(obj,i);
                %             h = get(gca,'Children');
                %             set(gca,'Children',[h(3) h(2) h(1)]);
                
                h = get(gca,'Children');
                set(gca,'Children',[h(1) h(2)]);
                
                % set marker points
                SetMarkerPoints(obj,i);
                %             obj.midlines_{i} = [obj.x_,obj.y_];
                
                % initialize frame interpolation with marker midlines
                obj.midlines_interp_{i} = obj.midlines_dense_{i};
                
                obj.current_ax_window_(1:2) = get(gca,'XLim');
                obj.current_ax_window_(3:4) = get(gca,'YLim');
            end
        end
        
    end
    
    %% Set markers to define the midline
    % i:        current frame number
    % xlimits:  1 x 2 array of axis limits in x-direction
    % ylimits:  1 x 2 array of axis limits in y-direction
    function SetMarkerPoints(obj,i)
        
        % reload (if available) or set impoints
        markers = [];
        for j = 1:obj.n_markers_
            if(i==obj.frame_list_(1) && isempty(obj.midlines_{i}))
                i_point(j) = impoint();
            elseif(~isempty(obj.midlines_{i}))
                i_point(j) = impoint(gca,...
                          [obj.midlines_{i}(j,1),obj.midlines_{i}(j,2)]);
            else
                i_point(j) = impoint(gca,[obj.x_(j),obj.y_(j)]);
            end
            i_point(j).setColor([0.9,0.2,0.2]);
            markers(j,:) = i_point(j).getPosition;
%             obj.x_(j) = markers(1);
%             obj.y_(j) = markers(2);

        end
        obj.midlines_{i} = markers;
%         obj.midlines_{i}(:,1) = obj.x_;
%         obj.midlines_{i}(:,2) = obj.y_;
        
        n_samples = 100;
        ComputeDenseMidline(obj,i,n_samples);
        DrawDenseMidline(obj,i);
        
        for j = 1:obj.n_markers_
            addNewPositionCallback(i_point(j),...
                @(p) UpdateMidline(obj,p,j,i));
        end
        title(['Frame: ' num2str(i) '/' num2str(obj.n_frames_) ...
                ' - modify points if needed']);

        pause;
        hold off;
        obj.x_ = obj.midlines_{i}(:,1);
        obj.y_ = obj.midlines_{i}(:,2);
    end
    
    %% Compute midline
    
    function ComputeDenseMidline(obj,frame_id,n_samples,mode)
        
        if(nargin<4)
            DenseMarkerInterpolation(obj,frame_id,n_samples);
        else
            DenseMarkerInterpolation(obj,frame_id,n_samples,mode);
        end
        obj.midlines_interp_{frame_id} = obj.midlines_dense_{frame_id};
        
    end
    
    %% Update Midline
    
    function UpdateMidline(obj,p,j,frame_id)
        
        obj.midlines_{frame_id}(j,:) = p;
        
        n_samples = 100;
        ComputeDenseMidline(obj,frame_id,n_samples);
        DrawDenseMidline(obj,frame_id);
        
    end
        
    
    %% (Dense) interpolation between markers
    % frame_id:    frame number
    function DenseMarkerInterpolation(obj,frame_id,n_samples,mode)
        
        % number of interpolation points
%         n_samples = 100;
        
        if(nargin<=3)
            interp_mode = 'spline';
        elseif(nargin==4)
            interp_mode = mode;
        end

        % midline markers
        x = obj.midlines_{frame_id}(:,1);
        y = obj.midlines_{frame_id}(:,2);
        
        % interpolation
        q_dense = linspace(0,1,n_samples);
        q = linspace(0,1,length(x));
        obj.midlines_dense_{frame_id} = [];
        obj.midlines_dense_{frame_id}(:,1) = interp1(q,x,q_dense,...
                                                     interp_mode);
        obj.midlines_dense_{frame_id}(:,2) = interp1(q,y,q_dense,...
                                                     interp_mode);
        
    end
    
    %% Distribute equidistantly 
    % For all frames, compute the equidistant markers along the dense
    % midline
    function EquidistantPoints(obj,n_points)
        
        obj.n_equi_points_ = n_points;
        
        for frame_id = 1:obj.n_frames_
            
            if(~isempty(obj.midlines_interp_{frame_id}))
                
                obj.midlines_equi_{frame_id} = [];
                
                x_dense = obj.midlines_interp_{frame_id}(:,1);
                y_dense = obj.midlines_interp_{frame_id}(:,2);
                
                % arc length along curve
                curve_length = sum(sqrt(diff(x_dense).^2+...
                    diff(y_dense).^2));
                equi_distance = curve_length/(n_points-1);
                cumsum_length = ...
                    [0,cumsum(sqrt(diff(x_dense).^2+diff(y_dense).^2))'];
                
                % find equidistant marker positions along
                % interpolated curve
                for i = 1:n_points-1
                    ind(i) = find(cumsum_length>=(i-1)*equi_distance,1);
                end
                ind(i+1) = length(cumsum_length);
                
                % store midline in class object
                obj.midlines_equi_{frame_id}(:,1) = x_dense(ind);
                obj.midlines_equi_{frame_id}(:,2) = y_dense(ind);
                
            end
            
        end
    end
    
    %% Average midline length
    
    function AverageMidlineLength(obj) 
        
        obj.midline_len_ = zeros(obj.n_frames_,1);
        for frame_id = 1:obj.n_frames_
            x_dense = obj.midlines_interp_{frame_id}(:,1);
            y_dense = obj.midlines_interp_{frame_id}(:,2);
            
            dx = diff(x_dense);
            dy = diff(y_dense);
            obj.midline_len_(frame_id) = sum(sqrt(dx.^2+dy.^2));
        end
        
        obj.avg_midline_len_ = mean(obj.midline_len_);
        
    end
    
    %% Interpolate midlines between defined frames
    
    function InterpolateMidlines(obj,frame_range)
        
        obj.frame_list_ = [];
        for i = 1:obj.n_frames_
            if(~isempty(obj.midlines_{i}))
               obj.frame_list_ = [obj.frame_list_, i];
            end
        end
        
        obj.midlines_interp_ = [];
        for i = obj.frame_list_
            obj.midlines_interp_{i} = obj.midlines_dense_{i};
        end
        
        for i = frame_range(1):frame_range(2)
            if(i>obj.frame_list_(1) && i<obj.frame_list_(end))
                
                prev_ref_frame = obj.frame_list_(...
                    find(i>=obj.frame_list_,1,'last'));
                next_ref_frame = obj.frame_list_(...
                    find(i<obj.frame_list_,1,'first'));
                
                weight = (i-prev_ref_frame)/...
                    (next_ref_frame-prev_ref_frame);
                
                x_prev = obj.midlines_dense_{prev_ref_frame}(:,1);
                y_prev = obj.midlines_dense_{prev_ref_frame}(:,2);
                
                x_next = obj.midlines_dense_{next_ref_frame}(:,1);
                y_next = obj.midlines_dense_{next_ref_frame}(:,2);
                
                x_new = (1-weight)*x_prev+weight*x_next;
                y_new = (1-weight)*y_prev+weight*y_next;
                
                obj.midlines_interp_{i} = [x_new,y_new];
            end
        end
        
    end

    %% Display boundary outlines or images
    
    function DisplayTrackingObject(obj,i)
        
        % display boundaries
        if(obj.mode_ == 1)
            plot(obj.bnd_outlines_{i}(:,1),...
                obj.bnd_outlines_{i}(:,2),...
                '-','linewidth',2);
        % display images
        elseif(obj.mode_ == 2)
            
        elseif(obj.mode_ == 3)
            imshow(obj.standard_images_{i});
        elseif(obj.mode_ == 4)
            imshow(obj.images_{i});
        end
        
        title(['Frame: ' num2str(i) '/' num2str(obj.n_frames_)]);
        hold on;
        axis equal;
        
        ax = gca;
        if(isempty(obj.current_ax_window_))
            xlimits = ax.XLim;
            ylimits = ax.YLim;
            
            xlim(xlimits + [-diff(xlimits) diff(xlimits)]*0.1);
            ylim(ylimits + [-diff(ylimits) diff(ylimits)]*0.1);
        else
            %         ax = gca;
            xlim(obj.current_ax_window_(1:2));
            ylim(obj.current_ax_window_(3:4));
        end
    end
    
    %% Display DaVis im7 images
    function DisplayDaVisImage(obj,i)
        showimx(obj.davis_data_.im7_{i});
        colormap gray;
    end
    
    %% Midline plot appearance
    
    function MidlinePlotAppearance(obj,markersize)
        obj.midline_plot_ = plot(0,0,'-or','linewidth',2,...
                                 'markersize',markersize,...
                                 'markerfacecolor','r');
    end

    %% Display the dense midline
    
    function DisplayAllMidlines(obj, midline_type, marker_size)
        
        fig = figure();
        fig.Position = [0 0 1440 1000];
        
        for frame_id = 1:1:obj.n_frames_%1:obj.n_frames_
            DisplayTrackingObject(obj,frame_id);

            MidlinePlotAppearance(obj,marker_size);
            
            if(midline_type == 1)
                DrawInterpFrameMidlines(obj,frame_id);
            elseif(midline_type == 2)
                DrawEquiMarkers(obj,frame_id);
            else
                error('midline type not defined!');
            end

            pause(0.5);
            hold off;
        end
        
    end

    %% Draw equidistant markers
    function DrawEquiMarkers(obj,frame_id)
        obj.x_ = obj.midlines_equi_{frame_id}(:,1);
        obj.y_ = obj.midlines_equi_{frame_id}(:,2);
        DrawMidline(obj);
    end
    
    %% Draw dense midline
    function DrawDenseMidline(obj,frame_id)
        obj.x_ = obj.midlines_dense_{frame_id}(:,1); 
        obj.y_ = obj.midlines_dense_{frame_id}(:,2);
        DrawMidline(obj);
    end
    
    %% Draw dense frame-interpolated midlines
    function DrawInterpFrameMidlines(obj,frame_id)
        obj.x_ = obj.midlines_interp_{frame_id}(:,1); 
        obj.y_ = obj.midlines_interp_{frame_id}(:,2);
        DrawMidline(obj);
    end
    
    %% Draw midline (e.g. dense, markers, etc.)
    function DrawMidline(obj)
        set(obj.midline_plot_,'xdata',obj.x_,'ydata',obj.y_);
    end
    
    %% Catch key press events
    function CatchKeyPress(obj,fig)
        set(fig,'KeyPressFcn',@KeyPressCb) ;
        function KeyPressCb(~,evnt)
            fprintf('key pressed: %s\n',evnt.Key);
            if strcmpi(evnt.Key,'f')

            elseif strcmpi(evnt.Key,'b')

            else
                evnt.Key
            end
        end
    end
    
end

end
