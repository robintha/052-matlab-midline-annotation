% Midline annotation and interploation

%% Load json-config file
config_text = fileread('config_example.json');
config_json = jsondecode(config_text);

input_path = [config_json.video_path config_json.video_name];

v = VideoReader(input_path);
n_frames = round(v.Duration * v.FrameRate);

frame_rate = 1/config_json.fps;
n_markers = config_json.n_markers;

frame_skip = config_json.frame_skip;

%% Initialization
disp('Initializing midline tracking class object .. .')
close all;
mode = 4; 
t = 0:frame_rate:frame_rate*(n_frames-1);
mid_annotation = MidlineAnnotation(mode, input_path, t, n_markers);

%% Annotation
disp('Annotating Midlines ...');
frame_list = 1:n_frames;
tracking_frame_list = ...
    [frame_list(1):frame_skip:frame_list(end),frame_list(end)];
mid_annotation.DefineMidlines(tracking_frame_list);

%% Interpolation
disp('Interpolating midlines between annotated frames ...');
frame_range = [frame_list(1),frame_list(end)];
mid_annotation.InterpolateMidlines(frame_range);

%% Display
disp('Displaying midlines ...');
close all;
midline_type = 1;
marker_size = 1;
mid_annotation.DisplayAllMidlines(midline_type,marker_size);

%% Set and apply image scale
% % via characteristic length in image
% frame_id = 1;
% calib_len = 10;
% mid_annotation.SetImageScale(frame_id,calib_len);
% 
% % or via fixed scale
% scale = 1/10;
% mid_annotation.SetImageScaleManually(scale);
% 
% % apply image scale to midline coordinates
% mid_annotation.ApplyImageScale();
% 
% % compute average midline length
% mid_annotation.AverageMidlineLength(); 


