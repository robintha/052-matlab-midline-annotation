### readme - midline annotation

1. Modify `config_example.json`:
    * `video_path`: path to folder that contains the video
    * `video_name`: name of the video
    * `fps`: frame rate (in frames per second) of the analyzed video,
    * `n_markers`: number of markers to define the midline,
    * `frame_skip`: number of frames to skip for annotation
2. Run `annotate.m`
3. `mid_annotation` contains midline coordinates in midlines_interp_
4. Optionally use functions `SetImageScale(calib_len)` [calib_len --> real world units, e.g., centimeters] or `SetImageScaleManually(scale)` [scale --> distance/pixel] to calibrate image. Use AverageMidlinelength() to get estimate of midline length averaged over all frames. Examples given in commented section at the end of `annotate.m`. 
